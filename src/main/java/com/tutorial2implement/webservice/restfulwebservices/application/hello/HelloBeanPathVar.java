package com.tutorial2implement.webservice.restfulwebservices.application.hello;

public class HelloBeanPathVar {
	private String message;

	public HelloBeanPathVar(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "HelloBeanPathVar [message=" + message + "]";
	}

}
