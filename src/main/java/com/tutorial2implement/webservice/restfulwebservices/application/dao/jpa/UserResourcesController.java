package com.tutorial2implement.webservice.restfulwebservices.application.dao.jpa;

import static org.springframework.hateoas.server.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.tutorial2implement.webservice.restfulwebservices.application.bean.User;
import com.tutorial2implement.webservice.restfulwebservices.application.dao.UserDaoService;
import com.tutorial2implement.webservice.restfulwebservices.application.exception.UserNotFoundException;
import com.tutorial2implement.webservice.restfulwebservices.application.exception.UserNotSavedException;
import com.tutorial2implement.webservice.restfulwebservices.application.filtering.SomeBean;

@RestController
public class UserResourcesController {

	@Autowired
	private UserRepository userRepository;
	
	@GetMapping(path = "/jpa/users")
	public List<User> getUsers() {
		List<User> usersList = userRepository.findAll();
		if (usersList.isEmpty()) {
			throw new UserNotFoundException("Users Not Found- ");
		}
		return usersList;
	}

	@GetMapping(path = "/jpa/users/{id}")
	public Optional<User> getOneUser(@PathVariable int id) {

		Optional<User> user = userRepository.findById(id);
		
		if (!user.isPresent())
			throw new UserNotFoundException("Not Found- " + id);

				return user;
	}

	@PostMapping(path = "/jpa/users")
	public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
		User u = userRepository.save(user);
		if (u.getId() == 0) {
			throw new UserNotSavedException("User not Created.");
		}
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(u.getId()).toUri();
		return ResponseEntity.created(location).build();

	}

	@DeleteMapping(path = "/jpa/users/{id}")
	public void deleteById(@PathVariable int id) {
		userRepository.deleteById(id);
		

	}

}
