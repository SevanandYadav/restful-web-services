package com.tutorial2implement.webservice.restfulwebservices.application.exception;

import java.util.Date;

public class ExceptionResponse {
	private Date timnestamp;
	private String message;
	private String details;
	public ExceptionResponse(Date timnestamp, String message, String details) {
		super();
		this.timnestamp = timnestamp;
		this.message = message;
		this.details = details;
	}
	public Date getTimnestamp() {
		return timnestamp;
	}
	public String getMessage() {
		return message;
	}
	public String getDetails() {
		return details;
	}


}
