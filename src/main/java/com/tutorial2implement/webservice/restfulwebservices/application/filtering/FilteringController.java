package com.tutorial2implement.webservice.restfulwebservices.application.filtering;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FilteringController {

	@GetMapping(path="/filteredBean")
	public SomeBean getFilterBean(){
		return new SomeBean("val1","val2","val3");
	}
}
