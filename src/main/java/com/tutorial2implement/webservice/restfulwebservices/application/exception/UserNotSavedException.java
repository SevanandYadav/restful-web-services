package com.tutorial2implement.webservice.restfulwebservices.application.exception;

public class UserNotSavedException extends RuntimeException {

	public UserNotSavedException(String message) {
		super(message);
	}

}
