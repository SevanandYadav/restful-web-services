package com.tutorial2implement.webservice.restfulwebservices.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tutorial2implement.webservice.restfulwebservices.application.bean.Post;
import com.tutorial2implement.webservice.restfulwebservices.application.dao.PostDaoService;

@RestController
public class PostController {
	@Autowired
	PostDaoService postDaoService;
	
	@GetMapping(path = "/users/{id}/posts/{post_id}")
	public void getUsersPost(@PathVariable Integer id, @PathVariable Integer post_id) {
		postDaoService.findOne(id,post_id);
	}

	@GetMapping(path = "/users/{id}/posts")
	public void getAllPost(@PathVariable Integer id) {
		postDaoService.findAll(id);
	}

	@PostMapping(path = "/users/{id}/posts")
	public void createUsersPost(@RequestBody Post post,@PathVariable Integer id) {
		postDaoService.save(post,id);
	}
}
