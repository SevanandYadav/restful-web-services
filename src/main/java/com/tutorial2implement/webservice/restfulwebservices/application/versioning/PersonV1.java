package com.tutorial2implement.webservice.restfulwebservices.application.versioning;

public class PersonV1 {
public PersonV1(String string) {
		this.name=string;
	}

private String name;

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

}
