/**
 * 
 */
package com.tutorial2implement.webservice.restfulwebservices.application.hello;

/**
 * @author Hp
 *
 */
public class HelloBean {
	private String message;

	public HelloBean(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "HelloBean [message=" + message + "]";
	}

}
