package com.tutorial2implement.webservice.restfulwebservices.application.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.tutorial2implement.webservice.restfulwebservices.application.bean.Post;
import com.tutorial2implement.webservice.restfulwebservices.application.bean.User;
@Component
public class PostDaoService {
	private static int postCount = 3;
	private static List<Post> postDetails = new ArrayList<>();


	public List<Post> findAll(Integer userID) {
		return postDetails;
	}

	public Post save(Post post,int userId) {
		if (post.getPostId() == null)
			post.setPostId(++postCount);
		postDetails.add(post);
		return post;
	}

	public Post findOne(int id,int post_id) {
		for (Post post : postDetails) {
			//user criteria
			if (post.getPostId() == id) {
				return post;
			}
		}

		return null;
	}
}
