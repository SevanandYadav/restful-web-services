package com.tutorial2implement.webservice.restfulwebservices.application.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.tutorial2implement.webservice.restfulwebservices.application.bean.User;
import com.tutorial2implement.webservice.restfulwebservices.application.exception.UserNotFoundException;
import com.tutorial2implement.webservice.restfulwebservices.application.hello.HelloBean;
import com.tutorial2implement.webservice.restfulwebservices.application.hello.HelloBeanPathVar;

@RestController
public class ApplicationController {

	
	
	@Autowired
	MessageSource resourceBundleMessageSource;


	@GetMapping(path = "/i18n")
	public String getMessagei18n(@RequestHeader(name="Accept-Language",required=false) Locale locale) {
		
		return resourceBundleMessageSource.getMessage("good.morning.message",null, locale);
	}
	@GetMapping(path = "/hello")
	public String getMessage() {
		return "Hello wORLD";
	}

	@GetMapping(path = "/hello-bean")
	public HelloBean getBeanMessage() {
		return new HelloBean("Hello wORLD");
	}

	@GetMapping(path = "/hello-bean/path-variable/{name}")
	public HelloBeanPathVar getBeanMessagePathVariable(@PathVariable String name) {
		return new HelloBeanPathVar(String.format("Hello wORLD, %s", name));
	}

}
