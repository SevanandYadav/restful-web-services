package com.tutorial2implement.webservice.restfulwebservices.application.versioning;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersionController {
//uri
	@GetMapping(path="/v1/person")
	public PersonV1 getPerson() {
		return new PersonV1("ravi chaddha");
	}
	@GetMapping(path="/v2/person")
	public PersonV2 getPerson2() {
		return new PersonV2(new Name("ravi","chaddha"));
	}
	//param
	@GetMapping(path="/person",params="version=1")
	public PersonV1 getPerson3() {
		return new PersonV1("ravi chaddha");
	}
	@GetMapping(path="/person",params="version=2")
	public PersonV2 getPerson4() {
		return new PersonV2(new Name("ravi","chaddha"));
	}
	//header
	@GetMapping(path="/person",headers="X-API-VERSION=1")
	public PersonV1 getPerson5() {
		return new PersonV1("ravi chaddha");
	}
	@GetMapping(path="/person",headers="X-API-VERSION=2")
	public PersonV2 getPerson6() {
		return new PersonV2(new Name("ravi","chaddha"));
	}
	//header accept/contentNegotiaiton
	@GetMapping(path="/person",produces="application/vnd.company.app-v1+json")
	public PersonV1 getPerson7() {
		return new PersonV1("ravi chaddha");
	}
	@GetMapping(path="/person",produces="application/vnd.company.app-v2+json")
	public PersonV2 getPerson8() {
		return new PersonV2(new Name("ravi","chaddha"));
	}
}
