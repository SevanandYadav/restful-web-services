package com.tutorial2implement.webservice.restfulwebservices.application.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

import com.tutorial2implement.webservice.restfulwebservices.application.bean.User;

@Component
public class UserDaoService {
	private static int userCount = 3;
	private static List<User> userDetails = new ArrayList<>();
	static {
		userDetails.add(new User(1, "Adam", new Date()));
		userDetails.add(new User(2, "Zampa", new Date()));
		userDetails.add(new User(3, "Anand", new Date()));
	}

	public List<User> findAll() {
		return userDetails;
	}

	public User save(User user) {
		if (user.getId() == null)
			user.setId(++userCount);
		userDetails.add(user);
		return user;
	}

	public User findOne(int id) {
		for (User user : userDetails) {
			if (user.getId() == id) {
				return user;
			}
		}

		return null;
	}

	public User deleteById(int id) {
		Iterator<User> users = userDetails.iterator();
		while (users.hasNext()) {
			User user = users.next();
			if (user.getId() == id) {
				users.remove();
				return user;
			}
		}

		return null;
	}

}
