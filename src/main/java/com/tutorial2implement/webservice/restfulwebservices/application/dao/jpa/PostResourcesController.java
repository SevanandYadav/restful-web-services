package com.tutorial2implement.webservice.restfulwebservices.application.dao.jpa;

import static org.springframework.hateoas.server.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.tutorial2implement.webservice.restfulwebservices.application.bean.Post;
import com.tutorial2implement.webservice.restfulwebservices.application.bean.User;
import com.tutorial2implement.webservice.restfulwebservices.application.dao.UserDaoService;
import com.tutorial2implement.webservice.restfulwebservices.application.exception.UserNotFoundException;
import com.tutorial2implement.webservice.restfulwebservices.application.exception.UserNotSavedException;
import com.tutorial2implement.webservice.restfulwebservices.application.filtering.SomeBean;

@RestController
public class PostResourcesController {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PostRepository postRepository;

	@GetMapping(path = "/jpa/users/{id}/posts")
	public List<Post> getOneUser(@PathVariable int id) {

		Optional<User> user = userRepository.findById(id);
		
		if (!user.isPresent())
			throw new UserNotFoundException("Not Found- " + id);
		

				return user.get().getPosts();
	}

	@PostMapping(path = "/jpa/users/{id}/posts")
	public ResponseEntity<Object> createUser(@PathVariable int id,  @RequestBody Post post) {
		Optional<User> u = userRepository.findById(id);
		if (!u.isPresent()) {
			throw new UserNotSavedException("User not Created.");
		}
		post.setUser(u.get());
		postRepository.save(post);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(post.getPostId()).toUri();
		return ResponseEntity.created(location).build();

	}



}
