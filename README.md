**Social Media Application : Demonstration of Restful webservices**

---

## Services provided related to USER


1. Create a user  --> POST /users
2. Retrive a user  --> GET /users/{id} -->/users/1
3. Delete a user  --> DELETE /users/{id} -->/users/1
4. Retrive all user   --> GET /users

##wORKING OF APPLICATION
User send the Post(s) to social media site

## Services provided related to USER Post
1. Create a post --> POST /users/{id}/posts
2. Retrive a post --> GET /users/{id}/posts/{POST_ID}
3. Retrive all posts--> GET /users/{id}/posts


## Miscellaneous Info/Resources
#REpresentational State Transfer REST
It is a software architecture for distributed application
Makes best use of HTTP: I.E METHODS(GET/POST/PUT/DELETE) and CODES

#Flow of control to return bean
1. Request comes to Specific controller through DispacherServlet
2.Bean Is returned By ResponseBody(part of RestController) as Json with help of jaskson & corresponding configuration HttpAutoConversion is taken care by Springboot-Auto-configuratoin


##Generic Exception  handling in Web services
Step 1. Create a Generic class that extends ResponseEntityExceptionHandler.
Step 2. Annotate the class with @RestController(as it will send response to client) & @ControllerAdvice (to intercept all controller's exception).
Step 3. Create method(s) signature similar to one in ResponseEntityExceptionHandler to handle Specific Exception . mark it with annotation @ExceptionHandler(Exception.class) where the parameter is type of exception it will handle.
Step 4. methods return tr=ype is Response Entity with take body and status as parameter,
body is supplied form ExceptionType being thrown and status-code as per requirement(HttpStatus.CODE_NAME) .

#Validation
from springboot 2.3.0 . javax.validation is not shipped with starter web
Need to add starter validation in POM

#HATEOAS - Hypermedia As The Engine Of Application State
when with data you want to show extar fields like(link to other resources) .. couldnot import classRe souces in hateos

Resources moved to -- > EntityModel
#Filtert=ing
Def: Send only selected dield from bean in response
Types:Static,Dynamic
Static:
@JsonIgnore (will not be part of json Reposne) at property level in bean
OR
@JsonIgnnoreProperty(value={"field1","fields2"})
BetterOne:
@JsonIgnore
No need to hardcode on classlevel. In case the variable name change no need to change at chall level

Dynamic: 
Using Class @ Controller leve : 
MapplingJacksonValue,FilterProvider,--i/P-->SimplePOJOXYXYXConverter

#Versioning
Ways:
part of url:
1.URI
2.REQUEST PARAM
Part of header:
3.custom header.
4.Content Negotition / Accept 


Which one to choose:
1.Http header missue
2.URI pollution in case if uri versioning
3.Caching not posiible for different api distingusable based on header


#REST Best Practices
-Richardson Maturity Model
 - 3 levels of matutity model 

Reference :in28Mins
